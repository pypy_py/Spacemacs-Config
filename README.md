# Spacemacs Dotfiles
It's what it says on the tin.

As of Feb. 2022, my setup is tailored for Python programming. I aim for the setup to be as boring as possible yet still have enough features for serious work.

Notable stuff in here:
* `ivy` in place of `helm`.
* `treemacs` enabled by default.
* Scrollbars are actually enabled.
* Ditch the distracting Spacemacs `mode-line` for the vanilla Emacs one.
* JetBrains Mono as the default font.
* `naysayer` (Jonathan Blow's theme) as the default theme.
* The option to use `ranger` as a file manager.
* `lsp-mode` with the GDScript, Python, TypeScript, Golang language server.
* `telega` to use Telegram inside Emacs.
